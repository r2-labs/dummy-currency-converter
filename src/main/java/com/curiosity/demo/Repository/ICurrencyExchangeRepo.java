package com.curiosity.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.curiosity.demo.Entity.CurrencyExchange;


public interface ICurrencyExchangeRepo  extends JpaRepository<CurrencyExchange, Long>, QueryByExampleExecutor<CurrencyExchange> {

}
