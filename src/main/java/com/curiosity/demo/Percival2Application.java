package com.curiosity.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Percival2Application {

	public static void main(String[] args) {
		SpringApplication.run(Percival2Application.class, args);
	}

}
