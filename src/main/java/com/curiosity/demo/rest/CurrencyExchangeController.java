package com.curiosity.demo.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curiosity.demo.Entity.Amount;
import com.curiosity.demo.Entity.CurrencyExchange;
import com.curiosity.demo.Service.CurrencyExchangeService;

import rx.Observable;
import rx.schedulers.Schedulers;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/converter")
public class CurrencyExchangeController {
	
	@Autowired
	private final CurrencyExchangeService converterService;
	private static final Logger log = LoggerFactory
			.getLogger(CurrencyExchangeController.class);

	public CurrencyExchangeController(CurrencyExchangeService converterService) {
		super();
		this.converterService = converterService;
	}

	@GetMapping(path = "/from/{origin}/to/{destiny}/amount/{amount}")
	public Observable<Amount> getOrder(@PathVariable String origin,
			                           @PathVariable String destiny,
			                           @PathVariable Double amount) {
		return converterService.convert(origin, destiny, amount)
			.subscribeOn(Schedulers.io());
	}
	
	@PostMapping
	public Observable<CurrencyExchange> postOrder(@RequestBody CurrencyExchange ce) {
		log.info(ce.toString());
		
		return converterService.createOrUpdate(ce).subscribeOn(Schedulers.io());
	}
}
