package com.curiosity.demo.Entity;

import javax.transaction.Transactional;

@Transactional
public class Amount {

	public Amount(Double amount, Double changeAmount, Double rate, String currencyOrigin, String currecyDestination) {
		super();
		this.amount = amount;
		this.changeAmount = changeAmount;
		this.rate = rate;
		this.currencyOrigin = currencyOrigin;
		this.currecyDestination = currecyDestination;
	}

	private Double amount;
	private Double changeAmount;
	private Double rate;
	private String currencyOrigin;
	private String currecyDestination;
	
	public Double getAmount() {
		return amount;
	}
	
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public Double getChangeAmount() {
		return changeAmount;
	}
	
	public void setChangeAmount(Double changeAmount) {
		this.changeAmount = changeAmount;
	}
	
	public String getCurrencyOrigin() {
		return currencyOrigin;
	}
	
	public void setCurrencyOrigin(String currencyOrigin) {
		this.currencyOrigin = currencyOrigin;
	}
	
	public String getCurrecyDestination() {
		return currecyDestination;
	}
	
	public void setCurrecyDestination(String currecyDestination) {
		this.currecyDestination = currecyDestination;
	}
	
	@Override
	public String toString() {
		return "Amount [amount=" + amount + ", changeAmount=" + changeAmount + ", currencyOrigin=" + currencyOrigin
				+ ", currecyDestination=" + currecyDestination + "]";
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}
	

	

}
