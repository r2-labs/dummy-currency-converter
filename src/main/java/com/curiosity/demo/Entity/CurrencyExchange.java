package com.curiosity.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="currecyExchange")
public class CurrencyExchange {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	// @Column(name = "origin", length = 50)
	private String currencyOrigin;
	
	// @Column(name = "destination", length = 50)
	private String currecyDestination;
	
	// @Column(name = "nombre", length = 50)
	private Double rate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getCurrencyOrigin() {
		return currencyOrigin;
	}

	public void setCurrencyOrigin(String currencyOrigin) {
		this.currencyOrigin = currencyOrigin;
	}

	public String getCurrecyDestination() {
		return currecyDestination;
	}

	public void setCurrecyDestination(String currecyDestination) {
		this.currecyDestination = currecyDestination;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		return "CurrencyExchange [ ID: " + id +"currencyOrigin=" + currencyOrigin + ", currecyDestination=" + currecyDestination
				+ ", rate=" + rate + "]";
	}


	private Double change(Double value) {
		return value * rate;
	}
}
