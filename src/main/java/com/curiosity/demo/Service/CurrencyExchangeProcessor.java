package com.curiosity.demo.Service;


import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.curiosity.demo.Entity.Amount;
import com.curiosity.demo.Entity.CurrencyExchange;
import com.curiosity.demo.Repository.ICurrencyExchangeRepo;

import rx.Observable;

@Service
public class CurrencyExchangeProcessor implements CurrencyExchangeService{

	@Autowired
	private ICurrencyExchangeRepo repo;
	
	private static final Logger log = LoggerFactory.getLogger(CurrencyExchangeProcessor.class);
	
	@SuppressWarnings("deprecation")
	@Override
	public Observable<Amount> convert(String origin, String destination, Double amount) {
		return Observable.<Amount>create(sub -> {
					
			Optional<CurrencyExchange> currencyExchange = getExchangeRate(origin, destination);

			if ( currencyExchange.isPresent()) {
				double rate = currencyExchange.get().getRate();
				double newAmount = amount * rate;
				double roundedAmount = (double) Math.round(newAmount * 100) / 100;
				sub.onNext(new Amount(amount, roundedAmount, rate, origin, destination));
			} else {
				sub.onError(null);
			}
			sub.onCompleted();
		})
		.doOnNext(p -> log.info("PROCESO CORRECTO "))
		.doOnError(e -> log.error("ERROR", e));
	}

	@SuppressWarnings("deprecation")
	@Override
	public Observable<CurrencyExchange> createOrUpdate(CurrencyExchange converter) {
		return Observable.<CurrencyExchange>create(sub -> {
			Optional<CurrencyExchange> optionalCurrencyExchange = getExchangeRate(
					converter.getCurrencyOrigin(), converter.getCurrecyDestination());
			if ( optionalCurrencyExchange.isPresent()) {
				CurrencyExchange exchange = optionalCurrencyExchange.get();
				exchange.setRate(converter.getRate());
				repo.save(exchange);
				sub.onNext(exchange);
			} else {
				repo.save(converter);
				sub.onNext(converter);
			}
			sub.onCompleted();
		})
		.doOnNext(p -> log.info("PROCESO CORRECTO "))
		.doOnError(e -> log.error("ERROR", e));
	}
	
	private Optional<CurrencyExchange>  getExchangeRate(String origin, String destination) {
		CurrencyExchange ce = new CurrencyExchange();
		ce.setCurrencyOrigin(origin);
		ce.setCurrecyDestination(destination);
		Example<CurrencyExchange> example = Example.of(ce);
		
		return repo.findOne(example);
	}


}
