package com.curiosity.demo.Service;

import com.curiosity.demo.Entity.Amount;
import com.curiosity.demo.Entity.CurrencyExchange;

import rx.Observable;

public interface CurrencyExchangeService {
	
	Observable<Amount> convert(String origin, String destiny, Double amount);

	Observable<CurrencyExchange> createOrUpdate(CurrencyExchange converter);

}
